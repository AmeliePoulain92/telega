// headerNavToggleBtn
headerNavToggleBtn.on('click', function(event) {
    event.preventDefault();
    $(this).toggleClass('active');
    headerNavList.toggleClass('active');
});

$(document).mouseup(function(event) {
    if (!headerNavToggleBtn.is(event.target) && headerNavToggleBtn.has(event.target).length === 0) {
    	headerNavToggleBtn.removeClass('active');
        headerNavList.removeClass('active');
    }
});
// END:headerNavToggleBtn
