// contact-js
jQuery.extend(jQuery.validator.messages, {
    required: "Поле не может быть пустым",
});
// contact-js-Name-Pattern
jQuery.validator.addMethod(
    'regexpName',
    function(value, element, regexp) {
        var re = new RegExp(regexp);
        return this.optional(element) || re.test(value);
    },
    "Пожалуйста, вводите только буквы"
);
// END:contact-js-Name-Pattern


contactForm.validate({
    // wrapper: "div class='error-wrapper'",
    errorPlacement: function(label, element) {
        $('<div class="error-wrapper"></div>').insertAfter(element).append(label);
    },
    rules: {
        name: {
            required: true,
            minlength: 2,
            regexpName: '^[a-zA-Zа-яА-Я_ ]+$'
        },
    },
    messages: {
        name: {
            minlength: 'Пожалуйста, введите как минимум {0} символа'
        },
        email: {
            email: 'Пожалуйста, введите корректный email-адрес'
        }
    },
    submitHandler: function() {
        contactModal.fadeIn(500);
    }
});

// contact-modal
$(document).mouseup(function(e) {
    if (!contactModalInner.is(e.target) && contactModalInner.has(e.target).length === 0) {
        contactModal.fadeOut(500);
    }
});
// END:contact-modal

// END:contact-js
